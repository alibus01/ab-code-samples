use std::cmp::Ordering;
use std::io;
use rand::Rng;

fn main() {
	const TRIES_MAX: u32 = 10;
	println!("Guess a number between 1 and 10 in {TRIES_MAX} tries");

	let ans = rand::thread_rng().gen_range(1..=10);
	let mut tries: u32 = 1;
	let mut guess: String;

	loop {
		println!("Input guess> ");
		guess = String::new();
		io::stdin()
			.read_line(&mut guess)
			.expect("failed to read line");

		let guess: u32 = match guess.trim().parse() {
			Ok(v) => v,
			Err(e) => {println!("err:[{e}], enter a number"); continue;},
		};

		match guess.cmp(&ans) {
			Ordering::Equal => {println!("Correct"); break;},
			Ordering::Less => println!("Higher"),
			Ordering::Greater => println!("Lower"),
		};

		if tries >= TRIES_MAX {
			println!("Out of tries");
			break;
		}
		tries += 1;
	}
	println!("answer:{ans} tries:{tries}");
}
